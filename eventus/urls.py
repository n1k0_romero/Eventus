from django.conf.urls import include, url, patterns
from django.contrib import admin
from eventus import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('apps.eventusApp.urls'), name="events_app"),
    url(r'', include('apps.login.urls'), name="login"),
    # url(r'', include('social.apps.django_app.urls'), name='social'),
    url(r'', include('social.apps.django_app.urls', namespace="social")),
    url(r'^uploads/(?P<path>.*)$','django.views.static.serve',{'document_root':settings.MEDIA_ROOT}),
]

if settings.DEBUG:
    import debug_toolbar
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
                            url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                                {'document_root': settings.MEDIA_ROOT,}
                                ),
                            url(r'^__debug__/', include(debug_toolbar.urls))
                            )