"""
Django settings for eventus project.

Generated by 'django-admin startproject' using Django 1.8.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
#unipath es una libreria como el pillow que hay que instalar en el settings
from unipath import Path
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#aqui le estamos diciendo que tome el directorio donde estamos y que decienda 3 niveles esto para que
# el MEDIA_ROOT encuentre las carpetas que se crearan con el nombre especico donde cada modelo sube las imagenes

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i^0ie0c=$bn8@8+fz!2*a%#=*p9nou)lc=_5z0cwi!j@5!*@k_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition
DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

LOCAL_APPS = (
    'apps.eventusApp',
    'apps.users'
)

THIRD_PARTY_APPS = (
    'reversion',
    'social.apps.django_app.default',
    'debug_toolbar',

)

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

MIDDLEWARE_CLASSES = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    # 'apps.eventusApp.middlewares.mymiddleware',
)

ROOT_URLCONF = 'eventus.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR, 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'apps.eventusApp.processors.myprocessor',
            ],
        },
    },
]

WSGI_APPLICATION = 'eventus.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'eventus',
        'USER': 'dante',
        'PASSWORD': 'dante',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

#variable para utilizar el modelo de usuario propio y no el de django
# AUTH_USER_MODEL = 'users.User'

#configuracion de archivos media
UPLOAD_TO = 'uploads'
MEDIA_ROOT = BASE_DIR + 'uploads'
MEDIA_URL = 'http://localhost:8000/uploads/'


AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookAppOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'django.contrib.auth.backends.ModelBackend',

)

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'

SOCIAL_AUTH_TWITTER_KEY = 'KIBBmOuO2Y79pManwh02SkyTY'
SOCIAL_AUTH_TWITTER_SECRET = '4agRzra3FN3mKlHn622brujdVVl3WX2FNlwPNDC9AyU5kOrGaE'

SOCIAL_AUTH_FACEBOOK_KEY = '444710315737662'
SOCIAL_AUTH_FACEBOOK_SECRET = '31bb9c9a75f9bd0ed301fb524bfa1e66'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'apps.users.pipelines.get_email',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    # 'social.pipeline.user.user_details',
    'apps.users.pipelines.user_details',
    'apps.users.pipelines.get_avatar',
)

PAYPAL_MODE = 'sandbox'
PAYPAL_CLIENT_ID = ''
PAYPAL_CLIENT_SECRET = ''


DEBUG_TOOLBAR_PATCH_SETTINGS = False
INTERNAL_IPS = ('127.0.0.1',)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}