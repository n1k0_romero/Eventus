from django.conf.urls import url
from .views import loginView, LogOutView


urlpatterns = [
    url(r'^login/$', 'apps.login.views.loginView', name="login"),
    url(r'^logOut/$', 'apps.login.views.LogOutView', name="logout"),


]