from django.contrib.auth import login, authenticate


def LogInFunction(request, username, password):
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            print(user)
            login(request, user)