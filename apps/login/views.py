from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import UserRegisterForm, LoginForm
from apps.login.functions import LogInFunction


def loginView(request):#la vista no puede llamarse jamas login (choca con el django)
    if request.method == 'POST':#si existe un metodo POST
        if 'register_form' in request.POST: #si existe el form con el nombre register_form (nombre del formulario en el input)
            user_register = UserRegisterForm(request.POST)#instanciamos lo que trae el form por post
            if user_register.is_valid():#definimos si es validoel formulario
                User.objects.create_user(username=user_register.cleaned_data['username'],
                                         email=user_register.cleaned_data['email'],
                                         password=user_register.cleaned_data['password'])#creamos usuario
                LogInFunction(request, username=user_register.cleaned_data['username'], #logeamos con la funcion en functions.py
                                       password=user_register.cleaned_data['password'])
                return redirect('/')

        if 'login_form' in request.POST:
            login_form = LoginForm(request.POST)
            if login_form.is_valid():
                LogInFunction(request, username=login_form.cleaned_data['username'], password=login_form.cleaned_data['password'])
                return redirect('/')

    else:
        user_register = UserRegisterForm()
        login_form = LoginForm()
    return render(request, 'login/login.html', {'user_register': user_register, 'login_form': login_form})


def LogOutView(request):
    logout(request)
    return redirect('/')