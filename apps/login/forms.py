from django import forms
from django.contrib.auth.models import User


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        widgets = {
            'username': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Ingresa un nombre de usuario'}),
            'email': forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Ingresa un email'}),
            'password': forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Ingresa una contraseña'}),
        }


class LoginForm(forms.Form):
    username = forms.CharField(max_length=20,
                               widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'usuario'}))
    password = forms.CharField(max_length=20,
                               widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'contraseña'}))
    # widgets = {
    #     'username':forms.TextInput(attrs={'class':'form-control', 'placeholder':'usuario'}),
    #     'password':forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'contraseña'}),
    # }
    # NOTA: Aqui no se puede utilizar widgets, por que es un forms.Form, solo se puede con forms.ModelForm
