# from django.contrib import admin
# from apps.users.models import User
# import reversion
# from apps.users.action_users import export_as_excel
#
# @admin.register(User)
# class UserAdmin(reversion.VersionAdmin):
#
#     list_display = ('username', 'first_name', 'last_name', 'email')
#     #para agregar un buscador en base alos campos que le demos
#     search_fields = ('username', 'email')
#     #para ordenarlo
#     ordering = ('username',)
#     #para agregar filtros en la barra derecha
#     list_filter = ('is_superuser',)
#     #para que los permisos de los grupos y permisos se agregen en vertical
#     filter_horizontal = ('groups', 'user_permissions')
#
#     #para exportar los usuarios a un excel el script esta en action_users.py
#     actions = [export_as_excel]
#     # modificamos el admin para que al agregar informacion este dividida en grupos
#     fieldsets = (
#         ('User', {'fields': ('username', 'password')}),
#         ('Personal Info', {'fields': ('first_name', 'last_name', 'email', 'avatar')}),
#         ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
#     )
