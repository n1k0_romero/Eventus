from django import forms
from .models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ('views', 'organizer', 'created', 'modified')
        widgets = {
                'name' : forms.TextInput(attrs={'class': 'form-control'}),
                'summary': forms.Textarea(attrs={'class': 'form-control', 'rows': '2'}),
                'content': forms.Textarea(attrs={'class': 'form-control', 'rows': '2'}),
                'category': forms.Select(attrs={'class': 'form-control'}),
                'place': forms.TextInput(attrs={'class': 'form-control'}),
                'start': forms.DateTimeInput(attrs={'class': 'form-control datepicker'}),
                'finish': forms.DateTimeInput(attrs={'class': 'form-control datepicker'}),
                'imagen': forms.ClearableFileInput(attrs={'class': 'form-control'}),
                'free': forms.CheckboxInput(attrs={'class': 'form-control'}),
                'amount': forms.NumberInput(attrs={'class': 'form-control'}),
        }


