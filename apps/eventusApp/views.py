from django.views.generic import TemplateView, FormView, DetailView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.cache import cache
from braces.views import LoginRequiredMixin
from .models import Event, Category
from .forms import EventForm


#vistas index/panel
class indexView(TemplateView):
    template_name = 'eventusApp/index.html'

    def get_context_data(self, **kwargs):
        context = super(indexView, self).get_context_data(**kwargs)
        context['events'] = Event.objects.all()
        context['category'] = Category.objects.all()
        return context


class PanelView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'eventusApp/panel.html'

    def get_context_data(self, **kwargs):
        context = super(PanelView, self).get_context_data(**kwargs)
        if not cache.get('events'):
            context['events'] = Event.objects.filter(organizer=self.request.user)
            cache.set('events', context['events'])
        else:
            context['events'] = cache.get('events')
        context['cantidad'] = context['events'].count()
        context['user'] = self.request.user
        return context


# vistas crear/actualizar/listar/eliminar/evento
class PanelEventNewCreateView(LoginRequiredMixin, FormView):
    login_url = 'login'
    form_class = EventForm
    model = Event
    template_name = 'eventusApp/create.html'
    success_url = reverse_lazy('panel')

    def get_success_url(self):
        return reverse('panel')

    def form_valid(self, form):
        form.instance.organizer = self.request.user
        form.save()
        messages.success(self.request, "Se ha Creado Satisfactoriamente el Evento")
        return super(PanelEventNewCreateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error en formulario")
        return super(PanelEventNewCreateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(PanelEventNewCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Crear Nuevo Evento'
        context['name'] = 'Crear Nuevo Evento'
        context['info'] = 'Ingresa los datos de tu evento'
        return context


class PanelEvenTDetailView(LoginRequiredMixin, DetailView):
    login_url = 'login'
    model = Event
    template_name = 'eventusApp/detail.html'

    def get_context_data(self, **kwargs):
        context = super(PanelEvenTDetailView, self).get_context_data(**kwargs)
        context['title'] = "Detalle de Evento"
        return context


class PanelEventUpdateView(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    form_class = EventForm
    model = Event
    template_name = 'eventusApp/create.html'
    success_url = reverse_lazy('panel')

    def get_context_data(self, **kwargs):
        context = super(PanelEventUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Actualizar Evento'
        context['name'] = 'Actulizar Evento'
        context['info'] = 'Aqui Puedes Actualizar tu Evento'
        return context

    def form_valid(self, form):
        messages.success(self.request, "Evento Actualizado Correctamente")
        return super(PanelEventUpdateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error en formulario")
        return super(PanelEventUpdateView, self).form_invalid(form)


class PanelEventDeleteView(LoginRequiredMixin, DeleteView):
    login_url = 'login'
    model = Event
    template_name = 'eventusApp/delete.html'
    success_url = reverse_lazy('panel')
    success_message = "El evento ha sido Eliminado con Exito"

    def delete(self, request, *args, **kwargs):
        messages.warning(self.request, self.success_message)
        return super(PanelEventDeleteView, self).delete(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PanelEventDeleteView, self).get_context_data(**kwargs)
        context['name'] = "Eliminar Evento"
        context['title'] = "Eliminar Evento"
        context['info'] = "Aqui puedes eliminar el evento"
        return context
