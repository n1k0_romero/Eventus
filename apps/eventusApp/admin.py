from django.contrib import admin
from apps.eventusApp.models import Event, Category, Comments, Assistant
import reversion


@admin.register(Event)
class EventAdmin(reversion.VersionAdmin):
    list_display = ('id', 'name', 'place', 'start')
    list_display_links = ('id', 'name', 'place', 'start')


@admin.register(Category)
class CategoryAdmin(reversion.VersionAdmin):
    list_display = ('name', 'slug')
    list_display_links = ('name', 'slug')


@admin.register(Comments)
class CommentsAdmin(reversion.VersionAdmin):
    list_display = ('user', 'event', 'content')
    list_display_links = ('user', 'event', 'content')


@admin.register(Assistant)
class AssistantAdmin(reversion.VersionAdmin):
    list_display = ('assistant',  'attended', 'paid')
    list_display_links = ('assistant',  'attended', 'paid')


