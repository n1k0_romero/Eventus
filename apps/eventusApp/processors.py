import django


def myprocessor(request):
    context = {'version': django.get_version()}
    return context
