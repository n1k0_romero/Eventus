from django.conf.urls import url
from django.views.decorators.cache import cache_page
from .views import indexView, PanelView, PanelEventNewCreateView, PanelEvenTDetailView, PanelEventUpdateView, \
    PanelEventDeleteView


urlpatterns = [
    url(r'^$', indexView.as_view(), name="index"),
    url(r'^panel/$', PanelView.as_view(), name="panel"),
    url(r'^panel/evento/nuevo$', PanelEventNewCreateView.as_view(), name="panel-create-event"),
    url('^panel/evento/(?P<pk>[0-9]+)/detail$', PanelEvenTDetailView.as_view(), name="panel-detail-event"),
    url('^panel/evento/(?P<pk>[0-9]+)/update$', PanelEventUpdateView.as_view(), name="panel-update-event"),
    url('^panel/evento/(?P<pk>[0-9]+)/delete$', PanelEventDeleteView.as_view(), name="panel-delete-event"),

]
