# from django.shortcuts import redirect
#
# class mymiddleware(object):
#
#     def process_request(self, request):
#         if request.user.is_authenticated():
#             if not request.user.is_superuser:
#                 paths = ['/']
#                 if request.path in paths:
#                     return None
#                 else:
#                     return redirect('/')
#             else:
#                 return None

# process_request es un metodo cargado en los mymiddleware que hace que intervengamos antes de que django decida
# que vista es la que ejecutara, hay mas procesos
#
# primero verificamos si esta autenticado el usuario
# verificamos si es superusuario
# si no es super usuario el unico lugar permitido es el raiz para que navege
# si la url(request.path) donde esta el usuario esta en la lista paths no hay accion
# si la url(request.path)donde esta el usario no esta en la lista, lo redirijimos
# si el usuario es super usuario no hay accion