from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from eventus.settings import UPLOAD_TO

#modelo para el control de quien crea y cuando crea
class TimeStampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, help_text="fecha de creacion")
    modified = models.DateTimeField(auto_now_add=True, help_text="fecha de modificacion")

    def __str__(self):
        return "%s %s %s" % (self.user_create, self.created, self.modified)

    class Meta:
        abstract = True

#modelo para darle categoria alos eventos
class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return "%s %s" % (self.name, self.slug)


#modelo del evento en si
class Event(TimeStampModel):
    name = models.CharField(max_length=100, unique=True, help_text="Cual es el nombre del evento")
    slug = models.SlugField(editable=False, help_text="slug")
    summary = models.TextField(max_length=300, help_text="De que tratara el Evento", null=True, blank=True)
    content = models.TextField(help_text="Contenido detallado del evento", null=True, blank=True)
    category = models.ForeignKey(Category, help_text="A que categoria pertenece el evento")
    place = models.CharField(max_length=50, help_text="lugar donde sera el evento", null=True, blank=True)
    start = models.DateTimeField(help_text="Cuando inicia el evento")
    finish = models.DateTimeField(help_text="Cuando Termina el Evento", null=True, blank=True)
    imagen = models.ImageField(upload_to=UPLOAD_TO, help_text="Proporcione una Imagen del Evento")
    free = models.BooleanField(default=True, help_text="Marque si es gratuito")
    amount = models.DecimalField(max_digits=5, decimal_places=2, default=0.00, help_text="precio del evento")
    views = models.PositiveIntegerField(default=0, help_text="cuantas vistas tiene el evento")
    organizer = models.ForeignKey(settings.AUTH_USER_MODEL)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Event, self).save(*args, **kwargs)

    def __str__(self):
        return "%s %s %s" % (self.name, self.category, self.amount)


#modelo de asistentes alos eventos
class Assistant(TimeStampModel):
    assistant = models.ForeignKey(settings.AUTH_USER_MODEL)
    event = models.ManyToManyField(Event, help_text="evento")
    attended = models.BooleanField(default=False, help_text="asistio")
    paid = models.BooleanField(default=False, help_text="pago por el evento")

    def __str__(self):
        return "%s %s %s" % (self.assistant, self.event, self.attended)


#modelo de comentarios para los eventos
class Comments(TimeStampModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    event = models.ForeignKey(Event)
    content = models.TextField()

    def __str__(self):
        return "%s %s" % (self.event, self.user_create)
